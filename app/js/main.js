$(document).ready(function(){

//hamburger
    $('#hamburger').click(function(){
        $(this).toggleClass('open');

    });

    $('main').click(function(){
        $('#hamburger').removeClass('open');
        $('#nav').removeClass('in');
    });



    $('.slider').slick({
        nextArrow: '<div class="slick-arrow-right"><span>&rang;</span></div>',
        prevArrow: '<div class="slick-arrow-left"><span>&lang;</span></div>',
        infinite: true,
        auto: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        // autoplay:true,

        responsive: [
            // {
            //   breakpoint: 1024,
            //   settings: {
            //     slidesToShow: 3,
            //     slidesToScroll: 3,
            //     infinite: true,
            //     dots: true
            //   }
            // },
            // {
            //   breakpoint: 600,
            //   settings: {
            //     slidesToShow: 2,
            //     slidesToScroll: 2
            //   }
            // },
            // {
            //   breakpoint: 480,
            //   settings: {
            //     slidesToShow: 1,
            //     slidesToScroll: 1
            //   }
            // }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]

    });

    $('.works-slider').slick({
        nextArrow: '<div class="slick-arrow-right"><span>&rang;</span></div>',
        prevArrow: '<div class="slick-arrow-left"><span>&lang;</span></div>',
        infinite: true,
        auto: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        // autoplay:true,

        responsive: [
            // {
            //   breakpoint: 1024,
            //   settings: {
            //     slidesToShow: 3,
            //     slidesToScroll: 3,
            //     infinite: true,
            //     dots: true
            //   }
            // },
            // {
            //   breakpoint: 600,
            //   settings: {
            //     slidesToShow: 2,
            //     slidesToScroll: 2
            //   }
            // },
            // {
            //   breakpoint: 480,
            //   settings: {
            //     slidesToShow: 1,
            //     slidesToScroll: 1
            //   }
            // }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]

    });


    $('.works-inner-slider').slick({
        nextArrow: '<div class="slick-arrow-right"><span>&rang;</span></div>',
        prevArrow: '<div class="slick-arrow-left"><span>&lang;</span></div>',
        infinite: true,
        auto: true,
        draggable: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        // autoplay:true,

        responsive: [
            // {
            //   breakpoint: 1024,
            //   settings: {
            //     slidesToShow: 3,
            //     slidesToScroll: 3,
            //     infinite: true,
            //     dots: true
            //   }
            // },
            // {
            //   breakpoint: 600,
            //   settings: {
            //     slidesToShow: 2,
            //     slidesToScroll: 2
            //   }
            // },
            // {
            //   breakpoint: 480,
            //   settings: {
            //     slidesToShow: 1,
            //     slidesToScroll: 1
            //   }
            // }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]

    });

    $('.works-inner-slider').on("mousedown mouseup", function() {
  	$('.works-slider').slick("slickGoTo", 1);
  })



// autoheight textarea
//     (function($)
// {
//     /**
//      * Auto-growing textareas; technique ripped from Facebook
//      *
//      *
//      * http://github.com/jaz303/jquery-grab-bag/tree/master/javascripts/jquery.autogrow-textarea.js
//      */
//     $.fn.autogrow = function(options)
//     {
//         return this.filter('textarea').each(function()
//         {
//             var self         = this;
//             var $self        = $(self);
//             var minHeight    = $self.height();
//             var noFlickerPad = $self.hasClass('autogrow-short') ? 0 : parseInt($self.css('lineHeight')) || 0;
//             var settings = $.extend({
//                 preGrowCallback: null,
//                 postGrowCallback: null
//               }, options );
//
//             var shadow = $('<div></div>').css({
//                 position:    'absolute',
//                 top:         -10000,
//                 left:        -10000,
//                 width:       $self.width(),
//                 fontSize:    $self.css('fontSize'),
//                 fontFamily:  $self.css('fontFamily'),
//                 fontWeight:  $self.css('fontWeight'),
//                 lineHeight:  $self.css('lineHeight'),
//                 resize:      'none',
//                 'word-wrap': 'break-word'
//             }).appendTo(document.body);
//
//             var update = function(event)
//             {
//                 var times = function(string, number)
//                 {
//                     for (var i=0, r=''; i<number; i++) r += string;
//                     return r;
//                 };
//
//                 var val = self.value.replace(/&/g, '&amp;')
//                                     .replace(/</g, '&lt;')
//                                     .replace(/>/g, '&gt;')
//                                     .replace(/\n$/, '<br/>&#xa0;')
//                                     .replace(/\n/g, '<br/>')
//                                     .replace(/ {2,}/g, function(space){ return times('&#xa0;', space.length - 1) + ' ' });
//
//                 // Did enter get pressed?  Resize in this keydown event so that the flicker doesn't occur.
//                 if (event && event.data && event.data.event === 'keydown' && event.keyCode === 13) {
//                     val += '<br />';
//                 }
//
//                 shadow.css('width', $self.width());
//                 shadow.html(val + (noFlickerPad === 0 ? '...' : '')); // Append '...' to resize pre-emptively.
//
//                 var newHeight=Math.max(shadow.height() + noFlickerPad, minHeight);
//                 if(settings.preGrowCallback!=null){
//                   newHeight=settings.preGrowCallback($self,shadow,newHeight,minHeight);
//                 }
//
//                 $self.height(newHeight);
//
//                 if(settings.postGrowCallback!=null){
//                   settings.postGrowCallback($self);
//                 }
//             }
//
//             $self.change(update).keyup(update).keydown({event:'keydown'},update);
//             $(window).resize(update);
//
//             update();
//         });
//     };
// })(jQuery);

// slick slide same height
    // var stHeight = $('#teachers .slick-track').height();
    // $('#teachers .slick-slide').css('height',stHeight + 'px' );
    //
    // var stHeight = $('#feedbacks .slick-track').height();
    // $('#feedbacks .slick-slide').css('height',stHeight + 'px' );


// phone mask
    $(function() {
        $('[name="phone"]').mask("+7(000)000-00-00", {
            clearIfNotMatch: true,
            placeholder: "+7(___)___-__-__"
        });
        $('[name="phone"]').focus(function(e) {
            if ($('[name="phone"]').val().length == 0) {
                $(this).val('+7(');
            }
        })
    });


//equal hieght
//     ;( function( $, window, document, undefined )
//     {
//         'use strict';
//
//         var $list       = $( '.achievement' ),
//             $items      = $list.find( '.achievement__item' ),
//             setHeights  = function()
//             {
//                 $items.css( 'height', 'auto' );
//
//                 var perRow = Math.floor( $list.width() / $items.width() );
//                 if( perRow == null || perRow < 2 ) return true;
//
//                 for( var i = 0, j = $items.length; i < j; i += perRow )
//                 {
//                     var maxHeight   = 0,
//                         $row        = $items.slice( i, i + perRow );
//
//                     $row.each( function()
//                     {
//                         var itemHeight = parseInt( $( this ).outerHeight() );
//                         if ( itemHeight > maxHeight ) maxHeight = itemHeight;
//                     });
//                     $row.css( 'height', maxHeight );
//                 }
//             };
//
//         setHeights();
//         $( window ).on( 'resize', setHeights );
//         $list.find( 'img' ).on( 'load', setHeights );
//     })( jQuery, window, document );
//
//
//
// // current month
//     var months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
//     var date = new Date();
//
//     document.getElementById('promoMonth').innerHTML = months[date.getMonth()];
//
//
// //form styler
//     //select
//     (function($) {
//         $(function() {
//             $('input, select').styler({
//
//             });
//         });
//     })(jQuery);

//form validation
    // send form
    // $('#messageForm1 .butn, #messageForm2 .butn, #messageForm3 .butn').click(function () {
    //     var parentClass = $(this).attr('rel');
    //     validate = 1;
    //     validate_msg = '';
    //     form = $('#' + $(this).attr('data-rel'));
    //     jQuery.each(form.find('.form-group input'), function (key, value) {
    //
    //         if ($(this).val() == '') {
    //             validate_msg += $(this).attr('title') + '\n';
    //             validate = 0;
    //             $(this).focus();
    //
    //             $(this).addClass('red_input');
    //
    //             $(this).keyup(function () {
    //
    //                 $(this).removeClass('red_input');
    //
    //             });
    //         }
    //
    //         else {
    //             $(this).removeClass('red_input');
    //         }
    //     });
    //
    //     if (validate == 1) {
    //         $.ajax({
    //             url: 'send.php'
    //             , data: 'action=send_form&' + form.serialize()
    //             , success: function (data) {
    //                 $('form').trigger('reset');
    //                 // $('#call-back, #request').modal('hide');
    //                 $('#thanks').modal('show');
    //             }
    //         });
    //     }
    //     else {}
    // });



});
